# Live Blog

Live Blog module allows to create dynamic page with new content coming in
without refreshing the page.
This module is useful for live events and streams.
This module allows to add/update/delete any post by admin and the final user's
page will get updated automatically.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/live_blog).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/live_blog).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1) Enable module "Live Blog" as usual of with drush: `drush en live_blog`
2) Use any content type or create a new one by /admin/structure/types/add
3) Push the button "Add field".
4) "Add a new field" => "Live Blog type" => Label: "Live Blog status"
5) Save => Help text: "Enable/Disable the status of Live Blog." => Save
6) Go to the "Manage display" page and disable "Label" for this New field.
7) Create content from step 2)
8) Enable the "Live Blog status" checkbox and Save.
9) Now you can add/update/delete any posts.

Multiple users can have that page opened and all content will be updated
automatically.


## Links

- Settings for Live Blog: /admin/structure/live-blog
- Add fields to Live Blog posts: /admin/structure/live-blog/fields
- List of all created Live Blogs: /admin/structure/live-blog/list
