<?php

/**
 * Implements hook_preprocess_HOOK().
 */
function live_blog_preprocess_live_blog_posts(&$variables) {
  // Event is Live classname.
  $variables['wrap_class'] = 'live-event-on';

  // Title.
  $variables['toolbar']['title'] = [
    '#children' => '<div class="live-blog-toolbar-title">' . $variables['node']->getTitle() . '</div>',
  ];

  // Play/Pause button.
  $variables['toolbar']['playpause'] = [
    '#children' => '<div class="live-blog-toolbar-playpause">
                      <input type="checkbox" id="playpause-' . $variables['node']->id() . '" />
                      <label for="playpause-' . $variables['node']->id() . '"></label>
                    </div>',
  ];

  // Save parent ID.
  $variables['parent_id'] = $variables['node']->id();
}

/**
 * Implements hook_preprocess_HOOK().
 */
function live_blog_preprocess_live_blog_post(&$variables) {
  // Get a post.
  $post = $variables['post']['#live_blog'];

  // Check access.
  if (!$post->access('view')) {
    $variables['attributes']['class'][] = 'live-blog-hidden';
    $variables['hidden'] = TRUE;
  }
  elseif (!$post->isPublished()) {
    $variables['attributes']['class'][] = 'live-blog-unpublished';
  }

  // Get username.
  $variables['name'] = $post->getOwner()->getDisplayName();

  // Save created time.
  $variables['time'] = \Drupal::service('date.formatter')->format($post->getCreatedTime(), 'custom', 'n/d/Y g:ia');

  // Save post ID.
  $variables['pid'] = $post->id();

  // Save post URL.
  $variables['entity_url'] = $variables['node']->toUrl('canonical', ['absolute' => TRUE]);
}
