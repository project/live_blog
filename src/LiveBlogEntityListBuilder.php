<?php

namespace Drupal\live_blog;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Live Blog entities.
 *
 * @ingroup live_blog
 */
class LiveBlogEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Post ID');
    $header['parent'] = $this->t('Parent node');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\live_blog\Entity\LiveBlogEntity $entity */
    $row['id'] = $entity->id();
    $row['parent'] = Link::createFromRoute(
      $entity->getParentTitle(),
      'entity.node.canonical',
      ['node' => $entity->getParentId()]
    );
    return $row + parent::buildRow($entity);
  }

}
