<?php

namespace Drupal\live_blog\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LiveBlogEntitySettingsForm.
 *
 * @ingroup live_blog
 */
class LiveBlogEntitySettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'live_blog.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [static::SETTINGS];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'live_blog_settings';
  }

  /**
   * Defines the settings form for Live Blog entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get settings.
    $settings = $this->config(static::SETTINGS)->get();

    $form['info']['#markup'] = 'Settings for Live Blog entities.';

    $form['interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Time interval'),
      '#default_value' => $settings['interval'],
      '#options' => [
        1 => t('1 sec'),
        3 => t('3 sec'),
        5 => t('5 sec'),
        10 => t('10 sec'),
        15 => t('15 sec'),
        30 => t('30 sec'),
        60 => t('1 min'),
      ],
      '#description' => $this->t('The time interval determines how often to update messages in the user\'s browser.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Call parent.
    parent::submitForm($form, $form_state);

    // Get config settings.
    $config = $this->config(static::SETTINGS);

    // Get values.
    $values = $form_state->getValues();

    // Save settings.
    $config->set('interval', $values['interval']);

    $config->save();
  }

}
