<?php

namespace Drupal\live_blog\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Live Blog entities.
 *
 * @ingroup live_blog
 */
class LiveBlogEntityDeleteForm extends ContentEntityDeleteForm {


}
