<?php

namespace Drupal\live_blog\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Live Blog entity revision.
 *
 * @ingroup live_blog
 */
class LiveBlogEntityRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Live Blog entity revision.
   *
   * @var \Drupal\live_blog\Entity\LiveBlogEntityInterface
   */
  protected $revision;

  /**
   * The Live Blog entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $liveBlogEntityStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->liveBlogEntityStorage = $container->get('entity_type.manager')->getStorage('live_blog');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'live_blog_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.live_blog.version_history', ['live_blog' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $live_blog_revision = NULL) {
    $this->revision = $this->liveBlogEntityStorage->loadRevision($live_blog_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->liveBlogEntityStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Live Blog entity: deleted %title revision %revision.', [
      '%title' => $this->revision->label(),
      '%revision' => $this->revision->getRevisionId(),
    ]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Live Blog entity %title has been deleted.', [
      '%revision-date' => \Drupal::service('date.formatter')->format($this->revision->getRevisionCreationTime()),
      '%title' => $this->revision->label(),
    ]));
    $form_state->setRedirect(
      'entity.live_blog.canonical',
       ['live_blog' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {live_blog_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.live_blog.version_history',
         ['live_blog' => $this->revision->id()]
      );
    }
  }

}
