<?php

namespace Drupal\live_blog\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Cache\Cache;

/**
 * Defines the Live Blog entity.
 *
 * @ingroup live_blog
 *
 * @ContentEntityType(
 *   id = "live_blog",
 *   label = @Translation("Live Blog"),
 *   handlers = {
 *     "storage" = "Drupal\live_blog\LiveBlogEntityStorage",
 *     "view_builder" = "Drupal\live_blog\Entity\LiveBlogEntityViewBuilder",
 *     "list_builder" = "Drupal\live_blog\LiveBlogEntityListBuilder",
 *     "views_data" = "Drupal\live_blog\Entity\LiveBlogEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\live_blog\Form\LiveBlogEntityForm",
 *       "add" = "Drupal\live_blog\Form\LiveBlogEntityForm",
 *       "edit" = "Drupal\live_blog\Form\LiveBlogEntityForm",
 *       "delete" = "Drupal\live_blog\Form\LiveBlogEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\live_blog\LiveBlogEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\live_blog\LiveBlogEntityAccessControlHandler",
 *   },
 *   base_table = "live_blog",
 *   revision_table = "live_blog_revision",
 *   revision_data_table = "live_blog_field_revision",
 *   translatable = FALSE,
 *   admin_permission = "administer live blog entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "parent_id" = "parent_id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/live-blog/list/{live_blog}",
 *     "add-form" = "/admin/structure/live-blog/list/add",
 *     "edit-form" = "/admin/structure/live-blog/list/{live_blog}/edit",
 *     "delete-form" = "/admin/structure/live-blog/list/{live_blog}/delete",
 *     "version-history" = "/admin/structure/live-blog/list/{live_blog}/revisions",
 *     "revision" = "/admin/structure/live-blog/list/{live_blog}/revisions/{live_blog_revision}/view",
 *     "revision_revert" = "/admin/structure/live-blog/list/{live_blog}/revisions/{live_blog_revision}/revert",
 *     "revision_delete" = "/admin/structure/live-blog/list/{live_blog}/revisions/{live_blog_revision}/delete",
 *     "collection" = "/admin/structure/live-blog/list",
 *   },
 *   field_ui_base_route = "live_blog.settings"
 * )
 */
class LiveBlogEntity extends EditorialContentEntityBase implements LiveBlogEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // If no revision author has been set explicitly,
    // make the live_blog owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Invalidate cache tags.
    self::invalidCache($this);

    // Save to log.
    live_blog_log_save($update ? 'update' : 'create', $this);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    if ($entities) {
      foreach ($entities as $entity) {
        // Invalidate cache tags.
        self::invalidCache($entity);

        // Save to log.
        live_blog_log_save('delete', $entity);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getParentId() {
    return $this->parent_id->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentId($parent_id) {
    $this->set('parent_id', $parent_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentNode() {
    return $this->parent_id->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentTitle() {
    // Get the Parent node.
    $node = $this->getParentNode();
    return $node ? $node->label() : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->created->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->user_id->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->user_id->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Live Blog entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['parent_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent node'))
      ->setDescription(t('The parent node to which this Live Blog post belongs to.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'node')
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'region' => 'hidden',
        'type' => 'number_integer',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -4,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => 'Try to find by Live Blog title',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Live Blog entity is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * Invalid cache.
   */
  public static function invalidCache(LiveBlogEntityInterface $entity) {
    // Invalidate cache tags.
    Cache::invalidateTags([
      'live-blog:id:' . $entity->id(),
    ]);
  }

}
