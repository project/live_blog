<?php

namespace Drupal\live_blog\Entity;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class for entity view builders.
 *
 * @ingroup live_blog
 */
class LiveBlogEntityViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
    $build = parent::view($entity, $view_mode, $langcode);

    // Get the current user.
    $user = \Drupal::currentUser();

    // Is admin permission.
    $is_admin = $user->hasPermission('administer live blog entities');

    // Init items.
    $items = [];

    // Get destination.
    $destination = Url::fromRoute('<current>')->toString();

    // Add destination (if known)
    if (stripos($destination, 'api/live-blog') !== FALSE && isset($_SERVER['HTTP_REFERER'])) {
      $destination = str_replace($GLOBALS['base_url'] . '/', '', $_SERVER['HTTP_REFERER']);
    }

    if ($is_admin || $user->hasPermission('edit live blog entities')) {
      // Edit button.
      $items[] = [
        '#type' => 'link',
        '#title' => t('edit'),
        '#url' => Url::fromRoute('entity.live_blog.edit_form', [
          'live_blog' => $entity->id(),
        ], [
          'query' => ['destination' => $destination],
        ]),
        '#options' => [
          'attributes' => [
            'class' => ['live-blog-edit'],
          ],
        ],
      ];
    }

    if ($is_admin || $user->hasPermission('delete live blog entities')) {
      // Delete button.
      $items[] = [
        '#type' => 'link',
        '#title' => t('delete'),
        '#url' => Url::fromRoute('entity.live_blog.delete_form', [
          'live_blog' => $entity->id(),
        ], [
          'query' => ['destination' => $destination],
        ]),
        '#options' => [
          'attributes' => [
            'class' => ['live-blog-delete'],
          ],
        ],
      ];
    }

    if ($items) {
      // Generate admin links.
      $build['links'] = [
        '#theme' => 'item_list',
        '#items' => $items,
        '#weight' => 99,
        '#attributes' => [
          'class' => ['live-blog-links', 'clearfix'],
        ],
      ];
    }

    return $build;
  }

}
