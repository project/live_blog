<?php

namespace Drupal\live_blog\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Live Blog entities.
 */
class LiveBlogEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
