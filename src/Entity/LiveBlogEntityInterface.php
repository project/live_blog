<?php

namespace Drupal\live_blog\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Live Blog entities.
 *
 * @ingroup live_blog
 */
interface LiveBlogEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Live Blog entity Parent ID.
   *
   * @return int
   *   Parent ID of the Live Blog entity.
   */
  public function getParentId();

  /**
   * Sets the Live Blog entity Parent ID.
   *
   * @param int $parent_id
   *   The Live Blog entity Parent ID.
   *
   * @return \Drupal\live_blog\Entity\LiveBlogEntityInterface
   *   The called Live Blog entity.
   */
  public function setParentId($parent_id);

  /**
   * Gets the Live Blog entity Parent node.
   *
   * @return object
   *   Parent node of the Live Blog entity.
   */
  public function getParentNode();

  /**
   * Gets the Live Blog entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Live Blog entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Live Blog entity creation timestamp.
   *
   * @param int $timestamp
   *   The Live Blog entity creation timestamp.
   *
   * @return \Drupal\live_blog\Entity\LiveBlogEntityInterface
   *   The called Live Blog entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Live Blog entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Live Blog entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\live_blog\Entity\LiveBlogEntityInterface
   *   The called Live Blog entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Live Blog entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Live Blog entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\live_blog\Entity\LiveBlogEntityInterface
   *   The called Live Blog entity.
   */
  public function setRevisionUserId($uid);

}
