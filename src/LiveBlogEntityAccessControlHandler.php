<?php

namespace Drupal\live_blog;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Live Blog entity.
 *
 * @see \Drupal\live_blog\Entity\LiveBlogEntity.
 */
class LiveBlogEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\live_blog\Entity\LiveBlogEntityInterface $entity */

    if ($account->hasPermission('administer live blog entities')) {
      AccessResult::allowed();
    }

    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished live blog entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published live blog entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit live blog entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete live blog entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add live blog entities');
  }

}
