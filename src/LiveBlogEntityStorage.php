<?php

namespace Drupal\live_blog;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\live_blog\Entity\LiveBlogEntityInterface;

/**
 * Defines the storage handler class for Live Blog entities.
 *
 * This extends the base storage class, adding required special handling for
 * Live Blog entities.
 *
 * @ingroup live_blog
 */
class LiveBlogEntityStorage extends SqlContentEntityStorage implements LiveBlogEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(LiveBlogEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {live_blog_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {live_blog_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
