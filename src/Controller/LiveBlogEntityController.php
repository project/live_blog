<?php

namespace Drupal\live_blog\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\live_blog\Entity\LiveBlogEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DateFormatter;

/**
 * Class LiveBlogEntityController.
 *
 *  Returns responses for Live Blog entity routes.
 */
class LiveBlogEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   */
  public function __construct(DateFormatter $dateFormatter,
                              Renderer $renderer) {
    $this->dateFormatter = $dateFormatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this class.
    return new static(
      // Load the service required to construct this class.
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Live Blog entity revision.
   *
   * @param int $live_blog_revision
   *   The Live Blog entity revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow(int $live_blog_revision) {
    $entityTypeManager = $this->entityTypeManager();
    $live_blog = $entityTypeManager->getStorage('live_blog')
      ->loadRevision($live_blog_revision);
    $view_builder = $entityTypeManager->getViewBuilder('live_blog');

    return $view_builder->view($live_blog);
  }

  /**
   * Page title callback for a Live Blog entity revision.
   *
   * @param int $live_blog_revision
   *   The Live Blog entity revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle(int $live_blog_revision) {
    $live_blog = $this->entityTypeManager()->getStorage('live_blog')
      ->loadRevision($live_blog_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $live_blog->label(),
      '%date' => $this->dateFormatter->format($live_blog->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Live Blog entity.
   *
   * @param \Drupal\live_blog\Entity\LiveBlogEntityInterface $live_blog
   *   A Live Blog entity object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(LiveBlogEntityInterface $live_blog) {
    $account = $this->currentUser();
    $live_blog_storage = $this->entityTypeManager()->getStorage('live_blog');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $live_blog->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all live blog entity revisions") || $account->hasPermission('administer live blog entities')));
    $delete_permission = (($account->hasPermission("delete all live blog entity revisions") || $account->hasPermission('administer live blog entities')));

    $rows = [];

    $vids = $live_blog_storage->revisionIds($live_blog);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\live_blog\LiveBlogEntityInterface $revision */
      $revision = $live_blog_storage->loadRevision($vid);
      $username = [
        '#theme' => 'username',
        '#account' => $revision->getRevisionUser(),
      ];

      // Use revision link to revisions that are not active.
      $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
      if ($vid != $live_blog->getRevisionId()) {
        $link = $this->l($date, new Url('entity.live_blog.revision', [
          'live_blog' => $live_blog->id(),
          'live_blog_revision' => $vid,
        ]));
      }
      else {
        $link = $live_blog->link($date);
      }

      $row = [];
      $column = [
        'data' => [
          '#type' => 'inline_template',
          '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
          '#context' => [
            'date' => $link,
            'username' => $this->renderer->renderPlain($username),
            'message' => [
              '#markup' => $revision->getRevisionLogMessage(),
              '#allowed_tags' => Xss::getHtmlTagList(),
            ],
          ],
        ],
      ];
      $row[] = $column;

      if ($latest_revision) {
        $row[] = [
          'data' => [
            '#prefix' => '<em>',
            '#markup' => $this->t('Current revision'),
            '#suffix' => '</em>',
          ],
        ];
        foreach ($row as &$current) {
          $current['class'] = ['revision-current'];
        }
        $latest_revision = FALSE;
      }
      else {
        $links = [];
        if ($revert_permission) {
          $links['revert'] = [
            'title' => $this->t('Revert'),
            'url' => Url::fromRoute('entity.live_blog.revision_revert', [
              'live_blog' => $live_blog->id(),
              'live_blog_revision' => $vid,
            ]),
          ];
        }

        if ($delete_permission) {
          $links['delete'] = [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('entity.live_blog.revision_delete', [
              'live_blog' => $live_blog->id(),
              'live_blog_revision' => $vid,
            ]),
          ];
        }

        $row[] = [
          'data' => [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ];
      }

      $rows[] = $row;
    }

    $build['live_blog_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
