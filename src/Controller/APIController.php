<?php

namespace Drupal\live_blog\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Render\Renderer;
use Drupal\live_blog\Entity\LiveBlogEntity;

/**
 * API Controller.
 */
class APIController extends ControllerBase {

  /**
   * Request stack.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  public $requestStack;

  /**
   * Renderer service.
   *
   * @var Drupal\Core\Render\Renderer
   */
  public $renderer;

  /**
   * Class constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   */
  public function __construct(RequestStack $requestStack,
                              Renderer $renderer) {
    $this->request = $requestStack->getCurrentRequest();
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this class.
    return new static(
      // Load the service required to construct this class.
      $container->get('request_stack'),
      $container->get('renderer')
    );
  }

  /**
   * Get method.
   */
  public function get($parent_id, $lid) {
    // Prepare AJAX response.
    $response = new AjaxResponse();

    // Load Post IDs from the Log.
    $logs = live_blog_log_load($parent_id, $lid);

    // Load Posts.
    $posts = LiveBlogEntity::loadMultiple(array_keys($logs));

    // Build HTML of all posts.
    foreach ($posts as $post) {
      // View a post.
      $view_builder = \Drupal::entityTypeManager()->getViewBuilder('live_blog');
      $live_blog = $view_builder->view($post);

      // Prepare post.
      $build = [
        '#theme' => 'live_blog_post',
        '#post' => $live_blog,
        '#node' => $post->getParentNode(),
      ];

      // Renter HTML.
      $logs[$post->id()]->html = $this->renderer->render($build);
    }

    // Generate wrap ID.
    $wrap_id = '.live-blog-posts[data-parent-id=' . $parent_id . ']';

    if (!empty($logs)) {
      foreach ($logs as $log) {
        switch ($log->action) {
          case 'create':
            // Check sort.
            if ($this->request->request->get('sort') == 'asc') {
              // Add after.
              $response->addCommand(new AppendCommand($wrap_id, $log->html));
            }
            else {
              // Add before.
              $response->addCommand(new PrependCommand($wrap_id, $log->html));
            }
            break;

          case 'update':
            // Replace.
            $response->addCommand(new ReplaceCommand('#live-blog-post-' . $log->id, $log->html));
            break;

          case 'delete':
            // Remove.
            $response->addCommand(new RemoveCommand('#live-blog-post-' . $log->id));
            break;
        }
      }
    }

    // Run submitFormSuccess.
    $response->addCommand(new InvokeCommand(NULL, 'completeLiveBlogAPI', [
      [
        'wrap' => $wrap_id,
        'lid' => $log->lid ?? $lid,
      ],
    ]));

    return $response;
  }

}
