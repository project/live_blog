<?php

namespace Drupal\live_blog\Plugin\migrate\source;

use Drupal\migrate_drupal\Plugin\migrate\source\d7\FieldableEntity;
use Drupal\migrate\Row;

/**
 * Live Blog Item source plugin.
 *
 * @MigrateSource(
 *   id = "d7_live_blog",
 *   source_module = "live_blog",
 * )
 */
class LiveBlogItem extends FieldableEntity {

  /**
   * Join string for getting current revisions.
   */
  const JOIN = "l.sid = ls.sid";

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('live_blog_item', 'l')
      ->fields('l')
      ->fields('ls', [
        'entity_type',
        'entity_id',
      ]);
    $query->innerJoin('live_blog_sid', 'ls', static::JOIN);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // Get Field API field values.
    $post_id = $row->getSourceProperty('pid');

    // Get Field API field values.
    foreach ($this->getFields('live_blog') as $field_name => $field) {
      $row->setSourceProperty($field_name, $this->getFieldValues('live_blog', $field_name, $post_id));
    }

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'pid' => $this->t('Primary Key: Unique Live Blog item Post ID.'),
      'sid' => $this->t('The Source ID to which these Live Blog posts.'),
      'uid' => $this->t('The {users}.uid who authored the post'),
      'created' => $this->t('The time that the Live Blog post was created, as a Unix timestamp.'),
      'changed' => $this->t('The time that the Live Blog post was changed, as a Unix timestamp.'),
      'status' => $this->t('The published status of a post. (0 = Not Published, 1 = Published).'),
      'language' => $this->t('The {languages}.language of this post.'),
      'sid' => $this->t('Unique Source ID.'),
      'entity_type' => $this->t('Entity type.'),
      'entity_id' => $this->t('Entity ID.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'pid' => [
        'type' => 'integer',
      ],
    ];
  }

}
