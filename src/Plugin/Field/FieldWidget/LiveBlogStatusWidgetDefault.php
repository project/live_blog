<?php

namespace Drupal\live_blog\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\BooleanCheckboxWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_node_type_widget_default' widget.
 *
 * @FieldWidget(
 *   id = "field_node_type_widget_default",
 *   module = "live_blog",
 *   label = @Translation("Live Blog widget"),
 *   field_types = {
 *     "field_live_blog_status"
 *   },
 *   multiple_values = FALSE
 * )
 */
class LiveBlogStatusWidgetDefault extends BooleanCheckboxWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'checkbox',
      '#default_value' => !empty($items[0]->value),
    ];

    // Override the title from the incoming $element.
    if ($this->getSetting('display_label')) {
      $element['value']['#title'] = $this->fieldDefinition->getLabel();
    }
    else {
      $element['value']['#title'] = $this->fieldDefinition->getSetting('on_label');
    }

    unset($element['value']['#title_display']);
    return $element;
  }

}
