<?php

namespace Drupal\live_blog\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'field_live_blog_status' field type.
 *
 * @FieldType(
 *   id = "field_live_blog_status",
 *   label = @Translation("Live Blog type"),
 *   category = @Translation("Live Blog"),
 *   module = "live_blog",
 *   description = @Translation("Enable/Disable the Live Blog."),
 *   default_widget = "field_node_type_widget_default",
 *   default_formatter = "field_live_blog_status_formatter_default"
 * )
 */
class LiveBlogStatus extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'size' => 'tiny',
          'description' => 'Enable/Disable status of Live Blog.',
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return empty($value);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('integer')
      ->setLabel(t('Live Blog enable 2'));

    return $properties;
  }

}
