<?php

namespace Drupal\live_blog\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of our "field_live_blog_status_formatter_default" formatter.
 *
 * @FieldFormatter(
 *   id = "field_live_blog_status_formatter_default",
 *   module = "live_blog",
 *   label = @Translation("Live Blog format"),
 *   field_types = {
 *     "field_live_blog_status"
 *   }
 * )
 */
class LiveBlogStatusFormatterDefault extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = $cache_tags = [];

    // Only collect allowed options if there are actually items to display.
    if ($items->count()) {
      // Get Node.
      $node = $items->getEntity();

      foreach ($items as $delta => $item) {
        if ($item->value) {
          /// Get all Live Blog posts by Parent ID.
          $posts = \Drupal::entityTypeManager()->getStorage('live_blog')->loadByProperties([
            'parent_id' => $node->id(),
          ]);

          // Get the current user.
          $user = \Drupal::currentUser();

          // Add button.
          if ($user->hasPermission('edit live blog entities') || $user->hasPermission('administer live blog entities')) {
            $elements[$delta]['#add_more'] = [
              '#type' => 'link',
              '#title' => t('Add Live Blog post'),
              '#url' => Url::fromRoute('entity.live_blog.add_form', [], [
                'query' => [
                  'destination' => Url::fromRoute('<current>')->toString(),
                  'parent_id' => $node->id(),
                ],
              ]),
              '#weight' => (-1) * count($posts),
              '#options' => [
                'attributes' => [
                  'class' => ['live-blog-add'],
                  'id' => 'live-blog-add',
                ],
              ],
            ];
          }

          // Save $node.
          $elements[$delta]['#node'] = $node;

          // Add theme.
          $elements[$delta]['#theme'] = 'live_blog_posts';

          $max_lid = 0;
          foreach ($posts as $key => $post) {
            // Get Live Blog.
            $view_builder = \Drupal::entityTypeManager()->getViewBuilder('live_blog');
            $live_blog = $view_builder->view($post);

            // Add weight.
            $live_blog['#weight'] = (-1) * $key;

            // Save a post.
            $elements[$delta]['#posts'][$key] = [
              '#theme' => 'live_blog_post',
              '#post' => $live_blog,
              '#node' => $node,
            ];

            // Get Log ID.
            $lid = live_blog_get_lid($post->id());

            // Get max Log ID.
            $max_lid = max($max_lid, $lid);

            // Add tag by Log ID.
            $cache_tags[] = 'live-blog:id:' . $post->id();
          }

          // Save max Log ID.
          $elements[$delta]['#lid'] = $max_lid;
        }
      }
    }

    // Add cache tags.
    $elements['#cache']['tags'] = $cache_tags;

    return $elements;
  }

}
