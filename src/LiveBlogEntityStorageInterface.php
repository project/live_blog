<?php

namespace Drupal\live_blog;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\live_blog\Entity\LiveBlogEntityInterface;

/**
 * Defines the storage handler class for Live Blog entities.
 *
 * This extends the base storage class, adding required special handling for
 * Live Blog entities.
 *
 * @ingroup live_blog
 */
interface LiveBlogEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Live Blog entity revision IDs for a specific Live Blog entity.
   *
   * @param \Drupal\live_blog\Entity\LiveBlogEntityInterface $entity
   *   The Live Blog entity.
   *
   * @return int[]
   *   Live Blog entity revision IDs (in ascending order).
   */
  public function revisionIds(LiveBlogEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Live Blog entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Live Blog entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
