(function ($) {
  var vars = {
        ajaxing: false
      },

      // Getter
      getSort = function() {
        return sessionStorage.getItem('live_blog_sort') || 'desc';
      },

      // Setter
      setSort = function(val) {
        sessionStorage.setItem('live_blog_sort', val);
      },

      // Execute AJAX
      executeAJAX = function(options) {
        if (!vars.ajaxing) {
          // Do only one AJAX at a time
          vars.ajaxing = true;

          // Prepare AJAX
          var ajaxObject = Drupal.ajax(options);

          // Execute AJAX
          ajaxObject.execute().done(function() {
            vars.ajaxing = false;
          });
        } else {
          setTimeout(function(){ executeAJAX(options); }, 100);
        }
      },

      // Do API call
      liveAPI = function(parent_id, lid) {
        if (!$('input[type="checkbox"][id=playpause-' + parent_id + ']').is(':checked')) {
          var settings = drupalSettings.live_blog,
              utcTime = Math.ceil(new Date().getTime() / 1000);

          // Do AJAX request
          executeAJAX({
            url: settings.api + '/' + parent_id + '/' + lid,
            base: false,
            element: false,
            progress: false,
            submit: {
              'sort': sort = getSort()
            }
          });
        }
      },

      // Update posts
      livePosts = function(that) {
        var parent_id = $(that).data('parent-id'),
            lid = $(that).data('lid');

        // Do API calls
        liveAPI(parent_id, lid);

        // Update posts in 5 sec
        setTimeout(function() { livePosts(that); }, 1000 * drupalSettings.live_blog.interval);
      },

      // Scroll to current post (if necessary)
      scroll = function() {
        var query = window.location.search.substring(1),
            vars = query.split("&");

        for (var i = 0; i < vars.length; i++) {
          var pair = vars[i].split('=');
          if (pair[0] == 'live-blog-post') {
            // Scroll to post
            $('html, body').stop().animate({ scrollTop: $('#live-blog-post-' + pair[1]).offset().top - 20 }, 1000);
            break;
          }
        }
      };

  // Prepare Live Blog posts
  Drupal.behaviors.liveBlog = {
    attach: function(context, settings) {
      $('.live-blog-wrap:not(.live-blog-wrap-processed)').each(function() {
        var wrap = $(this);

        // Update posts
        if ($(wrap).hasClass('live-event-on')) {
          livePosts($('.live-blog-posts', wrap));
        }

        var select = $('.live-blog-top-sort-select select', wrap),
            sort = getSort(), transition = 0;

        // Reorder posts
        $(select).change(function() {
          var posts = $('.live-blog-posts', wrap);

          $(posts).fadeOut(transition, function() {
            // Save settings
            setSort($(select).val());

            var posts = $('.live-blog-posts', wrap),
                weights = [], elements = [];

            // Create initial data-sort elements
            $('.live-blog-post', posts).each(function(index, el) {
              var weight = $(el).data('weight');
              weights.push(weight);
              elements[weight] = $(el).detach();
            });

            // Sort weights
            weights.sort(function(a, b) {
              return a - b;
            });

            for (var i in weights) {
              if (getSort() == 'desc') {
                $(posts).prepend(elements[weights[i]]);
              } else {
                $(posts).append(elements[weights[i]]);
              }
            }

            $(posts).fadeIn(transition);
          })
        })

        // Check settings
        if ($(select).val() != sort) {
          $(select).val(sort).change();
        }

        transition = 400;

        // Show posts
        $(wrap).fadeIn(transition, scroll);

        // Auto Show/Hide toolbar
        $(window).scroll(function() {
          var toolbar = $('.live-blog-toolbar', wrap);

          if ($(toolbar).hasClass('live-blog-toolbar-auto')) {
            var scrollTop = $(this).scrollTop(),
                top = $(wrap).offset().top || 200,
                height = $(wrap).outerHeight() || 0,
                windowHeight = $(window).height();

            if ((scrollTop + windowHeight - 200) > top && (!height || (scrollTop + windowHeight - top - 100) < height)) {
              $(toolbar).removeClass('live-blog-toolbar-closed')
            } else {
              $(toolbar).addClass('live-blog-toolbar-closed')
            }
          }
        });

        // Reply
        $(wrap).on('click', '.live-reply', function(e) {
          // Prevent links from clicking
          e.preventDefault();

          var quote = $(this), once = true;

          // Scroll to the form
          $('html, body').stop().animate({ scrollTop: $('#comments').offset().top - 150}, 1000, function() {
            if (once) {
              once = false;
              var pid = 'edit-comment-body-und-0-value-0';

              if (CKEDITOR && CKEDITOR.instances && CKEDITOR.instances[pid]) {
                // Update <textarea> from CKEDITOR
                CKEDITOR.instances[pid].updateElement();

                // Get text
                var old_text = $('textarea#' + pid).val().replace(/<p>(&nbsp; ?)*<\/p>/g, '').trim(),
                    new_text = $(quote).closest('.live-blog-post').find('.field-name-live-blog-body').text().trim();


                if (!old_text || confirm(Drupal.t("Unsubmitted information will be lost!"))) {
                  if (!new_text) {
                    new_text = $(quote).parent().find('.copy-link').attr('href');
                  }

                  if (new_text) {
                    new_text = '<p>[quote]</p><p>' + new_text + '</p><p>[/quote]</p>';
                  }

                  // Insert text into CKEDITOR
                  CKEDITOR.instances[pid].setData(new_text ? new_text : '');
                }
              }
            }
          });
        })

        // Close/Open
        $(wrap).on('click', '.live-blog-toolbar-control', function() {
          $(this).closest('.live-blog-toolbar')
            // Add or remove 'live-blog-toolbar-closed'
            .toggleClass('live-blog-toolbar-closed')
            // Disable auto mode
            .removeClass('live-blog-toolbar-auto');
        })

        // Add "live-blog-wrap-processed" class
        $(wrap).addClass('live-blog-wrap-processed');
      });
    }
  }

  // Complete Live Blog API
  $.fn.completeLiveBlogAPI = function(data) {
    var wrap = $(data.wrap);

    // Update last Log ID
    wrap.data('lid', data.lid);

    // Hide/Show empty message
    if ($('.live-blog-post', wrap).length) {
      $('.live-blog-empty', wrap).hide();
    }
    else {
      $('.live-blog-empty', wrap).show();
    }
  }

})(jQuery);
